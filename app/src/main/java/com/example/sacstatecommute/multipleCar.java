package com.example.sacstatecommute;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.sacstatecommute.Strings.subHeads;

public class multipleCar extends AppCompatActivity {
    private String Option = Strings.commuteTitle[1][0];
    TextView commuteDesc, permitDesc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_car);


        SharedPreferences sp = getSharedPreferences(settingsActivity.SETTINGS_PREF, Context.MODE_PRIVATE);
        String savedRole = sp.getString("role", "");

        commuteDesc = findViewById(R.id.commuteDesc);
        commuteDesc.setText(Strings.setDesc(Option));
        permitDesc= findViewById(R.id.permitText);
        permitDesc.setText(Strings.permitString[0]);



        EditText mpgInput = findViewById(R.id.MPGInput);
        EditText gasPriceInput = findViewById(R.id.gasInput);
        EditText insuranceInput = findViewById(R.id.insuranceInput);
        EditText mInput = findViewById(R.id.maintInput);
        EditText occupantInput = findViewById(R.id.occupantNoInput);


        RadioGroup permitGroup = (RadioGroup) findViewById(R.id.permitGroup);
        RadioButton semesterB = (RadioButton) findViewById(R.id.semButton);
        RadioButton summerB = (RadioButton) findViewById(R.id.summerButton);
        RadioButton weeklyB = (RadioButton) findViewById(R.id.weeklyButton);
        RadioButton dailyB = (RadioButton) findViewById(R.id.dailyButton);

        //permitGroup.setVisibility(View.INVISIBLE);
        Button s1 = findViewById(R.id.nextButton);
        Button c1 = findViewById(R.id.cButton);


        s1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                double mpgCost = 0.0;
                double gasCost = 0.0;
                double insuranceCost = 0.0;
                double maintCost = 0.0;
                int occupantNumber = 0;
                String permitChoice ="";


                if(mpgInput.getText().toString().equals("") ){
                    error(mpgInput, subHeads[0]);
                    return;
                } else if(gasPriceInput.getText().toString().equals("") ){
                    error(gasPriceInput, subHeads[1]);
                    return;
                } else if(insuranceInput.getText().toString().equals("") ){
                    error(insuranceInput, subHeads[2]);
                    return;
                }else if(mInput.getText().toString().equals("") ){
                    error(mInput, subHeads[3]);
                    return;
                }else if (occupantInput.getText().toString().equals("")){
                    error(occupantInput, subHeads[4]);
                    return;
                }

                mpgCost = Double.parseDouble(mpgInput.getText().toString());
                gasCost = Double.parseDouble(gasPriceInput.getText().toString());
                insuranceCost = Double.parseDouble(insuranceInput.getText().toString());
                maintCost = Double.parseDouble(mInput.getText().toString());
                occupantNumber = Integer.parseInt(occupantInput.getText().toString());



                int radioId = permitGroup.getCheckedRadioButtonId();
                RadioButton temp = findViewById(radioId);
                double permitCost = 0;
                try {
                    if (semesterB.isChecked()) {
                        permitChoice = (String) temp.getText();
                        if (savedRole.equals("Student")) {
                            permitCost = 178.00;
                            move(Option, mpgCost, gasCost, insuranceCost, maintCost, permitCost, permitChoice, occupantNumber);
                        } else if (savedRole.equals("faculty")) {
                            permitCost = 72.00;
                            move(Option, mpgCost, gasCost, insuranceCost, maintCost, permitCost, permitChoice, occupantNumber);
                        } else {
                            Toast.makeText(multipleCar.this, "Visitors cannot use semester permits", Toast.LENGTH_LONG).show();
                            semesterB.setChecked(false);
                        }
                    } else if (summerB.isChecked()) {
                        permitChoice = (String) temp.getText();
                        if (savedRole.equals("Student")) {
                            permitCost = 119.00;
                            move(Option, mpgCost, gasCost, insuranceCost, maintCost, permitCost, permitChoice, occupantNumber);
                        } else if (savedRole.equals("faculty")) {
                            permitCost = 48.00;
                            move(Option, mpgCost, gasCost, insuranceCost, maintCost, permitCost, permitChoice, occupantNumber);
                        } else {
                            Toast.makeText(multipleCar.this, "Visitors cannot use summer permits", Toast.LENGTH_LONG).show();
                            summerB.setChecked(false);
                        }
                    } else if (weeklyB.isChecked()) {
                        permitChoice = (String) temp.getText();
                        if (savedRole.equals("Student")) {
                            permitCost = 12.00;
                            move(Option, mpgCost, gasCost, insuranceCost, maintCost, permitCost, permitChoice, occupantNumber);
                        } else if (savedRole.equals("faculty")) {
                            permitCost = 4.80;
                            move(Option, mpgCost, gasCost, insuranceCost, maintCost, permitCost, permitChoice, occupantNumber);
                        } else {
                            Toast.makeText(multipleCar.this, "Visitors cannot use weekly permits", Toast.LENGTH_LONG).show();
                            semesterB.setChecked(false);
                        }
                    } else if (dailyB.isChecked()) {
                        permitChoice = (String) temp.getText();
                        permitCost = 7.00;

                        move(Option, mpgCost, gasCost, insuranceCost, maintCost, permitCost, permitChoice, occupantNumber);
                    } else {
                        throw new NullPointerException();
                    }
                } catch(NullPointerException e){
                    Toast.makeText(multipleCar.this, "You did not select a permit option.", Toast.LENGTH_LONG).show();
                }
            }
        });


        c1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
    private void move(String option, double mpgCost, double gasCost, double insuranceCost, double maintCost, double permitCost, String permitChoice, int occupantNumber) {
        Intent i = new Intent(multipleCar.this, resultsActivity.class);
        i.putExtra("OPTION", Strings.commuteTitle[1][0]);
        i.putExtra("MPG", mpgCost);
        i.putExtra("GAS", gasCost);
        i.putExtra("INSURANCE", insuranceCost);
        i.putExtra("MAINTENANCE", maintCost);
        i.putExtra("PERMIT", permitCost);
        i.putExtra("PERMITCHOICE", permitChoice);
        i.putExtra("OCCUPANT", occupantNumber);
        startActivity(i);
    }
    
    
    
    private void error(EditText a, String b){
        a.setError(b + " cannot be empty.");
    }

}