package com.example.sacstatecommute;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;



public class calculate {
    static final int CO2_EMISSION = 8887; //in grams

    static double carbonEmission(double a, double b){
        double mpg = a;
        double miles = b;

        //rounds the answer by two decimal places
        return (Math.round(((CO2_EMISSION / mpg ) * miles) * 100.0) / 100.0);
    }



    static double tripCost(double a, double b, double c) {
        double MPG = a;
        double miles = b;
        double gasPrice = c;

        return ( (miles / MPG) * gasPrice);
    }


    static String roundNumber(double a) {
         DecimalFormat d = new DecimalFormat("####.##");
         String b = d.format(a);
         return b;
    }


    static double twoWay(double a){
        return a * 2;
    }


    static double weekCost(double a, int b){
        return ((a * 2) * b);
    }


    static double monthCost(double a, int b){
        return ((a * 2) * b) * 4;
    }


    static double gramsToLbs(double a){
        return (a / 453.6);
    }
}
