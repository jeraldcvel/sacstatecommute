package com.example.sacstatecommute;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static java.lang.Math.round;

public class resultsActivity extends AppCompatActivity {

    TextView optionText, distanceText, costDescText, sustainabilityText, timeText;
    SharedPreferences sp;
    String costText, sustainText;
    Button bRoute, backB, restartB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        bRoute = findViewById(R.id.routeButton);
        backB = findViewById(R.id.resultBackB);
        restartB = findViewById(R.id.resultRestB);
        optionText = findViewById(R.id.optionText);
        distanceText = findViewById(R.id.walkText);
        costDescText = findViewById(R.id.costDescText);
        sustainabilityText = findViewById(R.id.carbonText);
        timeText = findViewById(R.id.timeText);



        sp = getSharedPreferences(settingsActivity.SETTINGS_PREF, Context.MODE_PRIVATE);
        float savedDistance = sp.getFloat("distance", (float) 0.00);
        int savedDays = sp.getInt("days", 1);
        distanceText.setText("Distance (in miles) - " + savedDistance + " miles");


        Intent intent = getIntent();
        String optionName = intent.getStringExtra("OPTION");
        optionText.setText("Commute Option:\n"+ optionName);

        setTimeArrival(savedDistance, optionName);


        if(optionName.equals("Single Occupant Vehicle") || optionName.equals("Motorcycle")) {
            Double MPG = intent.getDoubleExtra("MPG", 0);
            Double Gas = intent.getDoubleExtra("GAS", 0);
            Double insuranceCost = intent.getDoubleExtra("INSURANCE", 0);
            Double maintCost = intent.getDoubleExtra("MAINTENANCE", 0);
            Double permitCost = intent.getDoubleExtra("PERMIT", 0);
            String permitChoice = intent.getStringExtra("PERMITCHOICE");




            costText = Strings.occupantCostString(optionName,MPG, Gas, insuranceCost, maintCost, permitCost,
                    permitChoice,0,savedDistance, savedDays);

            costDescText.setText(costText);
            sustainText = Strings.sustainabilityString(optionName, MPG, savedDistance, 0);

            sustainabilityText.setText(sustainText);
        }

        if(optionName.equals("Multiple Occupant Vehicle")) {
            Double MPG = intent.getDoubleExtra("MPG", 0);
            Double Gas = intent.getDoubleExtra("GAS", 0);
            Double insuranceCost = intent.getDoubleExtra("INSURANCE", 0);
            Double maintCost = intent.getDoubleExtra("MAINTENANCE", 0);
            Double permitCost = intent.getDoubleExtra("PERMIT", 0);
            int occupantNumber = intent.getIntExtra("OCCUPANT", 2);
            String permitChoice = intent.getStringExtra("PERMITCHOICE");

            costText = Strings.occupantCostString(optionName, MPG, Gas, insuranceCost, maintCost, permitCost,
                    permitChoice,occupantNumber, savedDistance, savedDays);
            sustainText = Strings.sustainabilityString(optionName, MPG, savedDistance, occupantNumber);


            costDescText.setText(costText);
            sustainabilityText.setText(sustainText);
        }

        if(optionName.equals("Bus / Light Rail")){
            double MPG = 0.0;
            boolean transitChoice = intent.getBooleanExtra("transitChoice", true);
            double singleCost = intent.getDoubleExtra("singleCost", 0.00);
            double dailyCost = intent.getDoubleExtra("dailyCost",0.00);

            String savedRole = sp.getString("role", "");
            costText = Strings.busRailString(savedRole, dailyCost, savedDays);

            if(transitChoice == true){
                MPG = 3.6;
            }else
                MPG = 66.0;

            sustainText = Strings.sustainabilityString(optionName, MPG, savedDistance, 0);

            costDescText.setText(costText);
            sustainabilityText.setText(sustainText);
        }

        if(optionName.equals("Walk")){
            costText = Strings.walkString();
            costDescText.setText(costText);


        }
        if(optionName.equals("Bike")){
            costText = Strings.walkString();
            costDescText.setText(costText);
        }

        if(optionName.equals("RideShare (Uber/Lyft)")){
            String serviceOption = "";
            double fareCost = intent.getDoubleExtra("FARE", 0.00);
            double mileRateCost = intent.getDoubleExtra("MILE_RATE", 0.00);
            double minuteRate = intent.getDoubleExtra("MIN_COST", 0.00);
            double serviceCost = intent.getDoubleExtra("SERVICE", 0.00);
            boolean serviceChoice = intent.getBooleanExtra("SERVICE_CHOICE", true);

            if (serviceChoice == false){
                serviceOption = "lyft";
            }else {
                serviceOption = "Uber";
            }

            costText = Strings.rideShareString(serviceOption, fareCost, mileRateCost,minuteRate,serviceCost, savedDistance, savedDays);
            costDescText.setText(costText);

            sustainText = Strings.sustainabilityString(optionName, 25.4, savedDistance, 1);
            sustainabilityText.setText(sustainText);

        }


        bRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(resultsActivity.this, routeMap.class);
                i.putExtra("Option", optionName);
                startActivity(i);
            }
        });


        restartB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent (resultsActivity.this, commuteOptionsActivity.class);
                startActivity(i);
            }
        });



        backB.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



    }
    public void setTimeArrival(float savedDistance, String Option){
        double firstSpeed = 0.0, secondSpeed = 0.0;

        if(Option.equals("Single Occupant Vehicle") || Option.equals("Multiple Occupant Vehicle")
                || Option.equals("RideShare (Uber/Lyft)")){
            firstSpeed = 45.0;
            secondSpeed = 65.0;
        }else if( Option.equals("Bus / Light Rail")){
            firstSpeed = 13.6;
            secondSpeed = 66.0;
        } else if(Option.equals("Walk")){
            firstSpeed = 3.0;
            secondSpeed = 5.0;
        } else if(Option.equals("Bike")){
            firstSpeed = 12.0;
            secondSpeed = 16.0;
        }


        double timeResidential = Time.travelTime(savedDistance, firstSpeed);
        int resHour = Time.getHour(timeResidential);
        int resMin = Time.getMinute(timeResidential);
        double timeFreeway = Time.travelTime(savedDistance, secondSpeed);
        int freeHour = Time.getHour(timeFreeway);
        int freeMin = Time.getMinute(timeFreeway);

        String timeDesc = "Estimated time of arrival:\n" + freeHour + "H : " + freeMin + " min" +
                " to " + resHour + "H : " + resMin + "min";

        timeText.setText(timeDesc);
    }


}