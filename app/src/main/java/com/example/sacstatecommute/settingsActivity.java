package com.example.sacstatecommute;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.sacstatecommute.Strings.subHeads;

public class settingsActivity extends AppCompatActivity {
    public static final String SETTINGS_PREF = "settingsPrefs";


    String name, userRole, address;
    float distance;
    int days;
    int roleId;
    EditText nameInput, distanceInput, daysInput, addressInput;
    SharedPreferences sp;
    RadioGroup roleGroup;
    RadioButton roleButton, studentButton, facultyButton, visitorButton;
    Button saveButton, backButton;
    TextView t1, t2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        roleGroup = findViewById(R.id.roleGroup);
        studentButton = findViewById(R.id.studentB);
        facultyButton = findViewById(R.id.facultyB);
        visitorButton = findViewById(R.id.visitorB);
        t1 = findViewById(R.id.distanceInputText);
        t2 = findViewById(R.id.daysText);
        t1.setText("Enter the distance to campus in miles: ");
        t2.setText("How many days will you be coming to campus per week? Enter a number between 1 and 7:");


        nameInput = (EditText) findViewById(R.id.nameInput);
        distanceInput = (EditText) findViewById(R.id.distanceInput);
        daysInput = (EditText) findViewById(R.id.daysInput);
        addressInput = (EditText) findViewById(R.id.settingAddress);



        sp = getSharedPreferences(SETTINGS_PREF, Context.MODE_PRIVATE);
        String savedName = sp.getString("name", "");
        float savedDistance = sp.getFloat("distance", (float) 0.00);
        int savedDays = sp.getInt("days", 0);
        String savedAddress = sp.getString("address", "");
        String savedRole = sp.getString("role", "");
        if(savedRole.equals("Student")){
            studentButton.setChecked(true);
        }
        else if(savedRole.equals("Faculty")){
            facultyButton.setChecked(true);
        }
        else if(savedRole.equals("Visitor")){
            visitorButton.setChecked(true);
        }
        else{
            Toast.makeText(settingsActivity.this, "No role was selected previously", Toast.LENGTH_SHORT).show();
        }
        nameInput.setText(savedName);
        distanceInput.setText(""+savedDistance);
        daysInput.setText(""+savedDays);
        addressInput.setText(savedAddress);

        saveButton = findViewById(R.id.saveButton);
        backButton = findViewById(R.id.backButton);
        //saves the inputted field to a preference sheet
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            /*
                Check for blank inputs and return an error
             */
                if(nameInput.getText().toString().equals("") ){
                    error(nameInput, "Email");
                    return;
                } else if(distanceInput.getText().toString().equals("") ){
                    error(distanceInput, "Distance (in miles)");
                    return;
                } else if(daysInput.getText().toString().equals("") ){
                    error(daysInput, "Number of days ");
                    return;
                }

                name = nameInput.getText().toString();
                distance = Float.parseFloat(distanceInput.getText().toString());
                days = Integer.parseInt(daysInput.getText().toString());
                address = addressInput.getText().toString();
                if(addressInput.getText().toString().equals("")){
                    Toast.makeText(settingsActivity.this, "No address was selected", Toast.LENGTH_SHORT).show();
                }

                roleId = roleGroup.getCheckedRadioButtonId();
                roleButton = findViewById(roleGroup.getCheckedRadioButtonId());
                try{
                    if(studentButton.isChecked()){
                        userRole = roleButton.getText().toString();
                        saveData(name, distance, days, userRole, address);
                    } else if (facultyButton.isChecked()) {
                        userRole = roleButton.getText().toString();
                        saveData(name, distance, days, userRole, address);
                    } else if (visitorButton.isChecked()) {
                        userRole = roleButton.getText().toString();
                        saveData(name, distance, days, userRole, address);
                    } else {
                        throw new NullPointerException();
                    }
                }catch(NullPointerException e){
                    Toast.makeText(settingsActivity.this, "Select a role", Toast.LENGTH_SHORT).show();
                }
            }
        });


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void saveData(String name, float distance, int days, String userRole, String address) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("name", name);
        editor.putFloat("distance", distance);
        editor.putInt("days", days);
        editor.putString("role", userRole);
        editor.putString("address", address);
        editor.apply();
        Toast.makeText(settingsActivity.this, "Settings Saved", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }


    private void error(EditText a, String b){
        a.setError(b + " cannot be empty.");
    }
}