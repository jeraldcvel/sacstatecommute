package com.example.sacstatecommute;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class BusRail extends AppCompatActivity {
    TextView busRailDesc, busRailDesc2, dailyText, dailyInput, singleText, singleInput;
    RadioGroup ageGroup;
    RadioButton adultB, seniorB, disabilityB, k12StudentB;
    Button backB, submitB, busB, railB;
    boolean transitChoice;
    SharedPreferences sp;
    String [] busRailString = {
            "Since your role is student, you do not have to pay to use the transit services in Sacramento",
            "Since your role is faculty, you'll have to pay $40 for the semester to use the transit services in Sacramento",
            "Since your role is visitor, your price may vary to use the transit services"
    };

    double[][] transitFarePrice = {
            {2.50,7.00},    //Age 19-61
            {1.25,3.50}     //seniors, disability, k-12 students
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_rail);
        sp = getSharedPreferences(settingsActivity.SETTINGS_PREF, Context.MODE_PRIVATE);
        String savedRole = sp.getString("role", "");

        setViews();

        busB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitChoice = true;
               setDescVisibility(savedRole);
            }
        });
        railB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitChoice = false;
                setDescVisibility(savedRole);
            }
        });


        adultB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(2);
                dailyInput.setText(String.valueOf(transitFarePrice[0][1]));
                singleInput.setText(String.valueOf(transitFarePrice[0][0]));
            }
        });


        seniorB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(2);
                dailyInput.setText(String.valueOf(transitFarePrice[1][1]));
                singleInput.setText(String.valueOf(transitFarePrice[1][0]));
            }
        });


        disabilityB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(2);
                dailyInput.setText(String.valueOf(transitFarePrice[1][1]));
                singleInput.setText(String.valueOf(transitFarePrice[1][0]));
            }
        });


        k12StudentB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(2);
                dailyInput.setText(String.valueOf(transitFarePrice[1][1]));
                singleInput.setText(String.valueOf(transitFarePrice[1][0]));
            }
        });


        submitB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (savedRole.equals("Student") || savedRole.equals("Faculty")) {
                        move(savedRole, transitChoice, 0, 0);
                    } else if(savedRole.equals("Visitor")) {
                        double singleCost, dailyCost = 0.0;
                        singleCost = Double.parseDouble(singleInput.getText().toString());
                        dailyCost = Double.parseDouble(dailyInput.getText().toString());

                        move(savedRole, transitChoice, singleCost, dailyCost);
                    }else {
                        throw new NullPointerException();
                    }
                } catch(NullPointerException e) {
                    Toast.makeText(BusRail.this, "Something went wrong...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        backB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void move(String savedRole,boolean transitChoice, double a, double b){
        if(savedRole.equals("Student") || savedRole.equals("Faculty")){
            Intent i = new Intent(BusRail.this, resultsActivity.class);
            i.putExtra("OPTION",Strings.commuteTitle[3][0]);
            i.putExtra("transitChoice", transitChoice);
            startActivity(i);
        } else {
            double singleCost, dailyCost = 0.0;
            singleCost = Double.parseDouble(singleInput.getText().toString());
            dailyCost = Double.parseDouble(dailyInput.getText().toString());

            Intent i = new Intent(BusRail.this, resultsActivity.class);
            i.putExtra("OPTION",Strings.commuteTitle[3][0]);
            i.putExtra("transitChoice", transitChoice);
            i.putExtra("singleCost", singleCost);
            i.putExtra("dailyCost",dailyCost);
            startActivity(i);
        }
    }

    private void setViews() {
        busRailDesc = findViewById(R.id.busRailDesc);
        busRailDesc2 = findViewById(R.id.busRailDesc2);
        dailyText = findViewById(R.id.dailyText);
        dailyInput = findViewById(R.id.dailyInput);
        singleText = findViewById(R.id.singleText);
        singleInput = findViewById(R.id.singleInput);

        ageGroup = findViewById(R.id.ageGroup);
        adultB = findViewById(R.id.adultB);
        seniorB = findViewById(R.id.seniorB);
        disabilityB = findViewById(R.id.disabilityB);
        k12StudentB = findViewById(R.id.k12StudentB);

        backB = findViewById(R.id.busBackB);
        submitB = findViewById(R.id.busSubmit);

        setVisibility(0);

        busB = findViewById(R.id.busB);
        railB =findViewById(R.id.lightB);

        busRailDesc.setText(Strings.commuteTitle[3][1]);
    }

    public void setDescVisibility(String savedRole){
        busRailDesc2.setVisibility(View.VISIBLE);
        if(savedRole.equals("Student")){
            busRailDesc2.setText(busRailString[0]);
            backB.setVisibility(View.VISIBLE);
            submitB.setVisibility(View.VISIBLE);
        }else if (savedRole.equals("Faculty")){
            busRailDesc2.setText(busRailString[1]);
            backB.setVisibility(View.VISIBLE);
            submitB.setVisibility(View.VISIBLE);
        }else if(savedRole.equals("Visitor")){
            busRailDesc2.setText(busRailString[2]);
            setVisibility(1);
        }
    }

    public void setVisibility(int x){
        if(x == 0){
            busRailDesc2.setVisibility(View.INVISIBLE);
            ageGroup.setVisibility(View.INVISIBLE);
            dailyText.setVisibility(View.INVISIBLE);
            dailyInput.setVisibility(View.INVISIBLE);
            singleText.setVisibility(View.INVISIBLE);
            singleInput.setVisibility(View.INVISIBLE);
            backB.setVisibility(View.INVISIBLE);
            submitB.setVisibility(View.INVISIBLE);
        } else if (x == 1) {
            ageGroup.setVisibility(View.INVISIBLE);
        } else if(x == 2 ){
            dailyText.setVisibility(View.VISIBLE);
            dailyInput.setVisibility(View.VISIBLE);
            singleText.setVisibility(View.VISIBLE);
            singleInput.setVisibility(View.VISIBLE);
            backB.setVisibility(View.VISIBLE);
            submitB.setVisibility(View.VISIBLE);
        }
    }
}