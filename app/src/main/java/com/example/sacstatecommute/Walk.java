package com.example.sacstatecommute;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Walk extends AppCompatActivity {
    TextView walkDesc, distanceDesc, transitLink;
    private String [] textStrings = {
            "\nSince your distance to the campus is greater than one and a half miles," +
             " you may want to consider taking a look at some of the transit resources" +
              " that could be available to you.",
            "\nGreat! You are in a walkable distance to the campus! And if there better" +
                    " alternatives such as taking the bus along the way, feel free to take advantage of them!"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walk);


        SharedPreferences sp = getSharedPreferences(settingsActivity.SETTINGS_PREF, Context.MODE_PRIVATE);
        float savedDistance = sp.getFloat("distance", (float) 1.5);


        walkDesc = findViewById(R.id.walkDesc);
        distanceDesc = findViewById(R.id.walkText2);
        transitLink = findViewById(R.id.walkTransitLink);
        transitLink.setMovementMethod(LinkMovementMethod.getInstance());
        walkDesc.setText(Strings.commuteTitle[4][1]);

       String dText = "Your distance you inputted is: " + savedDistance + " miles.";
       if ( savedDistance > 1.5){
           dText = dText + textStrings[0];
       }
       else {
           dText = dText + textStrings[1];
       }
       distanceDesc.setText(dText);

        Button submit = findViewById(R.id.nextButton);
        Button cancel = findViewById(R.id.cButton);
        submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Walk.this, resultsActivity.class);
                i.putExtra("OPTION", Strings.commuteTitle[4][0]);
                startActivity(i);
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}