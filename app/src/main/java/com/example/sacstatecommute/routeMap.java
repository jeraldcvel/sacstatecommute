package com.example.sacstatecommute;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class routeMap extends AppCompatActivity {
    final public String SAC_STATE_ADDRESS = "6000 J St, Sacramento, CA 95819";
    EditText mapInitial, mapDestination;
    Button btRoute, mapBack;
    String travelMode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_map);

        mapInitial = findViewById(R.id.settingAddress);
        mapDestination = findViewById(R.id.destination_source);
        mapDestination.setText(SAC_STATE_ADDRESS);
        btRoute = findViewById(R.id.bt_route);
        mapBack = findViewById(R.id.mapBack);

        SharedPreferences sp = getSharedPreferences(settingsActivity.SETTINGS_PREF, Context.MODE_PRIVATE);
        String savedAddress = sp.getString("address", "");
        mapInitial.setText(savedAddress);


        Intent i = getIntent();
        String savedOption = i.getStringExtra("Option");
        getMode(savedOption);

        btRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String startPoint = mapInitial.getText().toString().trim();
                String campusAddress = SAC_STATE_ADDRESS.trim();

                if (startPoint.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter an Address", Toast.LENGTH_SHORT).show();
                } else {
                    DisplayTrack(startPoint, campusAddress);
                }
            }
        });


        mapBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void getMode(String Option){
        if(Option.equals(Strings.commuteTitle[0][0]) || Option.equals(Strings.commuteTitle[1][0]) || (Option.equals(Strings.commuteTitle[2][0]))){
            travelMode = "driving";
        }else if(Option.equals(Strings.commuteTitle[3][0])){
            travelMode = "transit";
        }else if(Option.equals(Strings.commuteTitle[4][0])){
            travelMode = "walking";
        }else if(Option.equals(Strings.commuteTitle[5][0])){
            travelMode = "bicycling";
        }else if(Option.equals(Strings.commuteTitle[6][0])){
            travelMode = "taXi";
        }
    }


    private void DisplayTrack(String startPoint, String campusAddress) {
        try {

            System.out.println("https://www.google.com/maps/dir/?api=1&origin=" + startPoint + "&destination=" + campusAddress +
                    "&travelmode=" + travelMode);
            Uri uri = Uri.parse("https://www.google.com/maps/dir/?api=1&origin=" + startPoint + "&destination=" + campusAddress +
                    "&travelmode=" + travelMode);

            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            i.setPackage("com.google.android.apps.maps");
            i.setFlags(i.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }catch(ActivityNotFoundException e){
            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps");
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            i.setFlags(i.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
    }
}