package com.example.sacstatecommute;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button startB = findViewById(R.id.startButton);
        startB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, commuteOptionsActivity.class);
                startActivity(i);
            }
        });
    }
    //Starts the app once the button is pressed; transitions to the Address Input page
    public void launchAddress(View v){
        Intent i = new Intent(this, settingsActivity.class);
        startActivity(i);
    }
}