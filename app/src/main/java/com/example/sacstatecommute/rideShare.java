package com.example.sacstatecommute;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class rideShare extends AppCompatActivity {
    TextView rideShareDesc, serviceDesc, fareText, mileCostText, minCostText, serviceCostText;
    TextView fareInput, mileCostInput, minCostInput, serviceCostInput;
    Button backB, submitB, lyftB, uberB, service1B, service2B, service3B, service4B,service5B;
    boolean serviceC;   //false for lyft/ true for Uber


    String [] serviceString = {
              "\nLyft\n\nLyft (base)\nBase Fare - $1.16 | Cost Per Mile - $0.80 | Cost per Minute - $0.22 | Service Fee $3.00 | Minimum fare - $2.90" +
               "\nLyft XL\nBase Fare - $1.50 | Cost Per Mile - $1.17 | Cost per Minute - $0.27 | Service Fee $3.10 | Minimum fare - $4.00" +
               "\nLyft LUX\nBase Fare - $4.50 | Cost Per Mile - $0.95 | Cost per Minute - $0.71 | Service Fee $3.10 | Minimum fare - $8.25" +
               "\nLyft LUX Black\nBase Fare - $8.00 | Cost Per Mile - $1.95 | Cost per Minute - $1.60 | Service Fee $2.60 | Minimum fare - $12.40" +
               "\nLyft LUX Black XL\nBase Fare - $14.75 | Cost Per Mile - $2.15 | Cost per Minute - $1.65 | Service Fee $2.60 | Minimum fare - $20.75",
                //uber
              "\nUber\n\nUberX\nBase fare - $1.15 | Cost Per Mile - $0.78 | Cost per Minute - $0.21 | Service fee - $3.00 | Minimum fare - $5.90" +
              "\nComfort\nBase Fare - $1.73 | Cost Per Mile - $0.90 | Cost per Minute - $0.31 | Service fee - $4.29 | Minimum fare - $10.50" +
               "\nXL\nBase Fare - $1.50 | Cost Per Mile - $1.17 | Cost per Minute - $0.27 | Service fee - $3.10 | Minimum fare - $7.10" +
               "\nSelect\nBase Fare - $4.32 | Cost Per Mile - $0.91 | Cost per Minute - $0.68 | Service fee - $3.10 | Minimum fare - $11.02" +
               "\nBlack\nBase Fare - $8.75 | Cost Per Mile - $1.96 | Cost per Minute - $1.60 | Service fee - $0.00 | Minimum fare - $15.75"
    };

    double [][][] servicePrice = {
            {//Lyft: base fare, cost per mile, cost per minute, service fee
                    {1.16,0.80,0.22,3.00},  //base
                    {1.50,1.17,0.27,3.10},  //XLF
                    {4.50,0.95,0.71,3.10},  //LUX
                    {8.00,1.95,1.60,2.60},  //LUX Black
                    {14.75,2.15,1.65,2.60}  //LUX Black XL
            },
            {//Uber: base fare, cost per mile, cost per minute, marketplace fee
                    {1.15,0.78,0.21,3.00},      //uberX
                    {1.73,0.90,0.31,4.29},      //comfort
                    {1.50,1.17,0.27,3.10},     //XL
                    {4.32,0.91,0.68,3.10},     //Select
                    {8.75,1.96,1.60,0.00}      //Black
            }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_share);

        setViews();

        uberB.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                setVisibility(1);
                serviceC = true;
                setButtons(serviceC);
                serviceDesc.setText(serviceString[1]);
            }
        });

        lyftB.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                setVisibility(1);
                serviceC = false;
                setButtons(serviceC);
                serviceDesc.setText(serviceString[0]);
            }
        });


        service1B.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(serviceC = true){
                    setVisibility(2);
                    fareInput.setText(String.valueOf(servicePrice[1][0][0]));
                    mileCostInput.setText(String.valueOf(servicePrice[1][0][1]));
                    minCostInput.setText(String.valueOf(servicePrice[1][0][2]));
                    serviceCostInput.setText(String.valueOf(servicePrice[1][0][3]));
                }else if(serviceC = false){
                    fareInput.setText(String.valueOf(servicePrice[0][0][0]));
                    mileCostInput.setText(String.valueOf(servicePrice[0][0][1]));
                    minCostInput.setText(String.valueOf(servicePrice[0][0][2]));
                    serviceCostInput.setText(String.valueOf(servicePrice[0][0][3]));
                }
            }
        });

        service2B.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(serviceC = true){
                    setVisibility(2);
                    fareInput.setText(String.valueOf(servicePrice[1][1][0]));
                    mileCostInput.setText(String.valueOf(servicePrice[1][1][1]));
                    minCostInput.setText(String.valueOf(servicePrice[1][1][2]));
                    serviceCostInput.setText(String.valueOf(servicePrice[1][1][3]));
                }else if(serviceC = false){
                    fareInput.setText(String.valueOf(servicePrice[0][1][0]));
                    mileCostInput.setText(String.valueOf(servicePrice[0][1][1]));
                    minCostInput.setText(String.valueOf(servicePrice[0][1][2]));
                    serviceCostInput.setText(String.valueOf(servicePrice[0][1][3]));
                }
            }
        });

        service3B.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(serviceC = true){
                    setVisibility(2);
                    fareInput.setText(String.valueOf(servicePrice[1][2][0]));
                    mileCostInput.setText(String.valueOf(servicePrice[1][2][1]));
                    minCostInput.setText(String.valueOf(servicePrice[1][2][2]));
                    serviceCostInput.setText(String.valueOf(servicePrice[1][2][3]));
                }else if(serviceC = false){
                    fareInput.setText(String.valueOf(servicePrice[0][2][0]));
                    mileCostInput.setText(String.valueOf(servicePrice[0][2][1]));
                    minCostInput.setText(String.valueOf(servicePrice[0][2][2]));
                    serviceCostInput.setText(String.valueOf(servicePrice[0][2][3]));
                }
            }
        });
        service4B.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(serviceC = true){
                    setVisibility(2);
                    fareInput.setText(String.valueOf(servicePrice[1][3][0]));
                    mileCostInput.setText(String.valueOf(servicePrice[1][3][1]));
                    minCostInput.setText(String.valueOf(servicePrice[1][3][2]));
                    serviceCostInput.setText(String.valueOf(servicePrice[1][3][3]));
                }else if(serviceC = false){
                    fareInput.setText(String.valueOf(servicePrice[0][3][0]));
                    mileCostInput.setText(String.valueOf(servicePrice[0][3][1]));
                    minCostInput.setText(String.valueOf(servicePrice[0][3][2]));
                    serviceCostInput.setText(String.valueOf(servicePrice[0][3][3]));
                }
            }
        });
        service5B.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(serviceC = true){
                    setVisibility(2);
                    fareInput.setText(String.valueOf(servicePrice[1][4][0]));
                    mileCostInput.setText(String.valueOf(servicePrice[1][4][1]));
                    minCostInput.setText(String.valueOf(servicePrice[1][4][2]));
                    serviceCostInput.setText(String.valueOf(servicePrice[1][4][3]));
                }else if(serviceC = false){
                    fareInput.setText(String.valueOf(servicePrice[0][4][0]));
                    mileCostInput.setText(String.valueOf(servicePrice[0][4][1]));
                    minCostInput.setText(String.valueOf(servicePrice[0][4][2]));
                    serviceCostInput.setText(String.valueOf(servicePrice[0][4][3]));
                }
            }
        });

        submitB.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                double fareCost, mileRateCost, minRateCost, serviceCost = 0.00;
                fareCost = Double.parseDouble(fareInput.getText().toString());
                mileRateCost = Double.parseDouble(mileCostInput.getText().toString());
                minRateCost = Double.parseDouble(minCostInput.getText().toString());
                serviceCost = Double.parseDouble(serviceCostInput.getText().toString());

                move(fareCost, mileRateCost, minRateCost, serviceCost, serviceC);

            }
        });


        backB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }



    private void move(double fareCost, double mileRateCost, double minRateCost, double serviceCost, boolean serviceC) {
        Intent i = new Intent(rideShare.this,resultsActivity.class);
        i.putExtra("OPTION", Strings.commuteTitle[6][0]);
        i.putExtra("FARE", fareCost);
        i.putExtra("MILE_RATE", mileRateCost);
        i.putExtra("MIN_COST", minRateCost);
        i.putExtra("SERVICE", serviceCost);
        i.putExtra("SERVICE_CHOICE", serviceC);
        startActivity(i);
    }



    private void setViews(){
        rideShareDesc = findViewById(R.id.rideShareDesc);
        serviceDesc = findViewById(R.id.serviceText);
        uberB = findViewById(R.id.uberB);
        lyftB =findViewById(R.id.lyftB);


        fareText = findViewById(R.id.fareText);
        mileCostText = findViewById(R.id.mileCostText);
        minCostText = findViewById(R.id.minCostText);
        serviceCostText = findViewById(R.id.serviceCostText);

        fareInput = findViewById(R.id.fareInput);
        mileCostInput = findViewById(R.id.mileCostInput);
        minCostInput = findViewById(R.id.minCostInput);
        serviceCostInput = findViewById(R.id.serviceCostInput);

        backB = findViewById(R.id.rideshareBackB);
        submitB = findViewById(R.id.rideShareSubmit);

        service1B = findViewById(R.id.service1B);
        service2B = findViewById(R.id.service2B);
        service3B = findViewById(R.id.service3B);
        service4B = findViewById(R.id.service4B);
        service5B = findViewById(R.id.service5B);

        setVisibility(0);




        rideShareDesc.setText(Strings.commuteTitle[6][1]);
    }

    private void setButtons(boolean a){
        if (a == false){
            service1B.setText("Lyft");
            service2B.setText("XL");
            service3B.setText("LUX");
            service4B.setText("LUX Black");
            service5B.setText("LUX Black XL");
        } else if(a == true){
            service1B.setText("UberX");
            service2B.setText("Comfort");
            service3B.setText("XL");
            service4B.setText("Select");
            service5B.setText("Black");
        }
    }

    private void setVisibility(int a) {
        if (a == 0 ){
            serviceDesc.setVisibility(View.INVISIBLE);
            service1B.setVisibility(View.INVISIBLE);
            service2B.setVisibility(View.INVISIBLE);
            service3B.setVisibility(View.INVISIBLE);
            service4B.setVisibility(View.INVISIBLE);
            service5B.setVisibility(View.INVISIBLE);

            fareText.setVisibility(View.INVISIBLE);
            mileCostText.setVisibility(View.INVISIBLE);
            minCostText.setVisibility(View.INVISIBLE);
            serviceCostText.setVisibility(View.INVISIBLE);

            fareInput.setVisibility(View.INVISIBLE);
            mileCostInput.setVisibility(View.INVISIBLE);
            minCostInput.setVisibility(View.INVISIBLE);
            serviceCostInput.setVisibility(View.INVISIBLE);

            backB.setVisibility(View.INVISIBLE);
            submitB.setVisibility(View.INVISIBLE);
        }
        if (a == 1) {
            serviceDesc.setVisibility(View.VISIBLE);
            service1B.setVisibility(View.VISIBLE);
            service2B.setVisibility(View.VISIBLE);
            service3B.setVisibility(View.VISIBLE);
            service4B.setVisibility(View.VISIBLE);
            service5B.setVisibility(View.VISIBLE);
        } else if (a == 2){
            fareText.setVisibility(View.VISIBLE);
            mileCostText.setVisibility(View.VISIBLE);
            minCostText.setVisibility(View.VISIBLE);
            serviceCostText.setVisibility(View.VISIBLE);

            fareInput.setVisibility(View.VISIBLE);
            mileCostInput.setVisibility(View.VISIBLE);
            minCostInput.setVisibility(View.VISIBLE);
            serviceCostInput.setVisibility(View.VISIBLE);

            backB.setVisibility(View.VISIBLE);
            submitB.setVisibility(View.VISIBLE);

        }
    }
}