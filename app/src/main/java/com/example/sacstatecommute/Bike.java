package com.example.sacstatecommute;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class Bike extends AppCompatActivity {
    TextView bikeDesc1, bikeDesc2;
    RadioButton yesB, noB;
    Button peakAdvButton, cancelB, nextB;
    int bikeBought = 0;

    private String[] bikeDescString = {
            "Since you already own a bike, make sure to take advantage " +
                    "of ASI Peak Adventures services. They offer repairs, " +
                    "maintenance, tune-ups, accessories and parts," +
                    " lockers for your bikes, and much more! Click on the button" +
                    " to launch their website to learn more.",
            "Take a look at ASI Peak Adventure shop where they offer bikes at with a " +
                    "student discount aswell as services ranging from repairs and tune-ups to lockers" +
                    " for your bikes to keep them safe while you're at campus. Click on the " +
                    "button to open their website to learn more!"

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike);

        bikeDesc1 = findViewById(R.id.bikeDesc);
        bikeDesc2 = findViewById(R.id.bikeShopDesc);
        bikeDesc2.setVisibility(View.INVISIBLE);
        peakAdvButton = findViewById(R.id.peakLinkButton);
        peakAdvButton.setVisibility(View.INVISIBLE);
        cancelB = findViewById(R.id.bikeCancelB);
        nextB = findViewById(R.id.bikeSubmitB);

        yesB = findViewById(R.id.yesB);
        noB =findViewById(R.id.noB);


        bikeDesc1.setText(Strings.commuteTitle[5][1]);
        yesB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bikeBought = 1;
                bikeDesc2.setVisibility(View.VISIBLE);
                peakAdvButton.setVisibility(View.VISIBLE);
                bikeDesc2.setText(bikeDescString[0]);
            }
        });
        noB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bikeBought = 2;
                bikeDesc2.setVisibility(View.VISIBLE);
                peakAdvButton.setVisibility(View.VISIBLE);
                bikeDesc2.setText(bikeDescString[1]);
            }
        });

        peakAdvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.peakadventures.org/bike-shop"));
                startActivity(urlIntent);
            }
        });

        nextB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bikeBought == 1 || bikeBought == 2){
                    move(bikeBought);
                }else {
                    Toast.makeText(Bike.this, "Did not indicate if bike is owned", Toast.LENGTH_LONG).show();
                }
            }
        });

        cancelB.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void move(int bikeBought) {
        Intent i = new Intent(Bike.this, resultsActivity.class);
        i.putExtra("bikeBought", bikeBought);
        i.putExtra("OPTION", "Bike");
        startActivity(i);
    }




}