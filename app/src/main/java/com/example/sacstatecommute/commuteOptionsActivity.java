package com.example.sacstatecommute;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class commuteOptionsActivity extends AppCompatActivity {
    RadioGroup radioGroup;
    Button buttonNext, buttonBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commute_options);

        radioGroup = findViewById(R.id.commuteGroup);

        /*
        declares the buttons created in the XML file
         */
        final RadioButton singleV = (RadioButton) findViewById(R.id.singleVehicleButton);
        final RadioButton multiV = (RadioButton) findViewById(R.id.multipleVehicleButton);
        final RadioButton motorcycle = (RadioButton) findViewById(R.id.motorcycleButton);
        final RadioButton busRail = (RadioButton) findViewById(R.id.busRailButton);
        final RadioButton bike = (RadioButton) findViewById(R.id.bikeButton);
        final RadioButton walk = (RadioButton) findViewById(R.id.walkButton);
        final RadioButton rideShare = (RadioButton) findViewById(R.id.rideShareButton);


        buttonNext = findViewById(R.id.nextButton);
        buttonBack = findViewById(R.id.back1Button);


        buttonNext.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                //Moves to the appropriate activity based on the user's choice
                try {
                    if (singleV.isChecked()) {   //single occupant vehicle
                        Intent i = new Intent(commuteOptionsActivity.this, singleCar.class);
                        startActivity(i);
                    } else if (multiV.isChecked()) { //multiple occupant vehicle
                        Intent i = new Intent(commuteOptionsActivity.this, multipleCar.class);
                        startActivity(i);
                    } else if (motorcycle.isChecked()) { //motorcycle
                        Intent i = new Intent(commuteOptionsActivity.this, Motorcycle.class);
                        startActivity(i);
                    } else if (busRail.isChecked()) { //bus and light rail
                        Intent i = new Intent(commuteOptionsActivity.this, BusRail.class);
                        startActivity(i);
                    } else if (walk.isChecked()) { //walk
                        Intent i = new Intent(commuteOptionsActivity.this, Walk.class);
                        startActivity(i);
                    } else if (bike.isChecked()) { //bike
                        Intent i = new Intent(commuteOptionsActivity.this, Bike.class);
                        startActivity(i);
                    } else if (rideShare.isChecked()) { //ride share / uber / lyft / etc.
                        Intent i = new Intent(commuteOptionsActivity.this, rideShare.class);
                        startActivity(i);
                    } else {
                        /*  if no choice was inputted, puts a toast message to try again to avoid
                            the app to crash.
                         */
                        throw new NullPointerException();
                    }
                } catch(NullPointerException e) {
                    Toast.makeText(commuteOptionsActivity.this, "You did not select an option. Try Again.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

}