package com.example.sacstatecommute;

public class Strings {

    public static String[][] commuteTitle = {
            {"Single Occupant Vehicle", "Driving seems to the popular choice, but is it the best choice? " +
                    "\tLet's see the emission and costs when we choose to drive ourselves keeping MPG, insurance" +
                    ", maintenance of your vehicle, and parking permit in mind."},
            {"Multiple Occupant Vehicle", "Having multiple occupants in a single vehicle is much cheaper" +
                    " and contributes to less traffic. By carpooling, you'll be emitting much less" +
                    " carbon emissions compared to if every occupant drove to campus separately." +
                    "\n\nCarpooling spaces are located at Parking Structures 1, 3, and 5."},
            {"Motorcycle", "Motorcycle is much faster vehicle to drive than other vehicles. However," +
                    " carpooling is still a much better option since you are emitting much less" +
                    " carbon dioxide into the air."},
            {"Bus / Light Rail", "Bus and light rail is the cheapest option than most of the others" +
                    " available. While bus may have a low MPG, it can reduce with traffic congestion " +
                    " as more people take advantage of public transportation, while the light rail" +
                    " does not emit any carbon dioxide. However, time may be significantly longer " +
                    " depending on the number of stops and distance.\n\n" +
                    "Students may ride all Sacramento Regional Transit (RT) services including light rail" +
                    " with their Sacramento State OneCard with the commuter sleeve.\nEmployees may also use " +
                    "this service with a 6-month employee commuter sleeve for $40."},
            {"Walk", "Walking is a great option since you don't have to worry about other factors that" +
                    " may cost money such as gas and insurance. However, if you live more than 1.5 miles" +
                    " or takes you more than 30 mins, it may not be worth to just walk since it will be" +
                    " physically exhausting."},
            {"Bike", "Bike is a much faster option than just walking! In fact, cycling is a much faster way" +
                    " to lose weight than walking since tre, burning twice as much calories compared to walking. " +
                    "\n\nDo you own a bike at the moment?"},
            {"RideShare (Uber/Lyft)", "Ridesharing apps such as Uber and Lyft can save you costs of owning" +
                    " a car yourself, and save you time compared to a few other commute options. This may be ideal" +
                    "if you cannot either drive, walk, or ride a bike yourself.\n" +
                    "However, ridesharing may increase the traffic congestion, carbon emissions, and decrease" +
                    " the use of public transportation services.\n\nWhich service will you be using?"}
    };


    public static String[] permitString = {
            //permit pricing for vehicles
            "\nSemester Permit\nStudent - $178.00\nFaculty - $72.00\nVisitor - N/A\n" +
                    "\nSummer Permit\nStudent - $119.00\nFaculty - $48.00\nVisitor -N/A\n" +
                    "\nWeekly Permit\nStudent - $12.00\nFaculty - 4.80\nVisitor - N/A\n" +
                    "\nDaily Parking\nEveryone(Student/Faculty/Visitors) - $7.00",
            "Permit Type" + //permit pricing for motorcycles
                    "\nSemester Permit\nStudent - $44.00\nFaculty - $18.00\nVisitor - N/A" +
                    "\nSummer Permit\nStudent - $30.00\nFaculty - $12.00\nVisitor - N/A" +
                    "\nWeekly Permit\nStudent - $12.00\nFaculty - 4.80\nVisitor - N/A" +
                    "\nDaily Parking\nEveryone(Student/Faculty/Visitors)- $7.00"
    };


    public static String[] subHeads = {"MPG",
            "Gas Price",
            "Insurance cost",
            "Maintenance cost",
            "Number of occupants"
    };

    private static String[] costString = {
            "Gas Cost for two trips - \t\t\t\t\t\t\t$",
            "days a week to campus - \t\t\t\t$",
            "\nEvery month will cost - \t\t\t\t\t\t\t\t$",
            "\nInsurance per month - \t\t\t\t\t\t\t\t$",
            "\nMaintenance cost per month -\t$",
            "\n\nYou will be spending $",
            " per month.",
            "\n\nThe semester total will be - $",
            "Because you chose to walk to campus, it will be free! Just watch yourself since it may be" +
                    " tiresome depending how far it is and the weather may impact your way to travel to " +
                    "campus. Stay safe!"
    };


    public static String occupantCostString(String option, double MPG, double Gas, double insuranceCost, double maintCost,
                                            double permitCost, String permitChoice, int occupantNumber, double savedDistance, int savedDays) {
        String s = "";
        String mTotal = "";
        double temp = calculate.tripCost(MPG, savedDistance, Gas);
        String twoWay = calculate.roundNumber(calculate.twoWay(temp)); //return the cost from point a to the campus
        String weekCost = calculate.roundNumber(calculate.weekCost(temp, savedDays));  //two-way-cost * # of days they go to campus
        String monthCost = calculate.roundNumber(calculate.monthCost(temp, savedDays));
        double tempGasCost = Double.parseDouble(monthCost);


        if (permitChoice.equals("Weekly Permit")) {
            permitCost = permitCost * 4;
            mTotal = calculate.roundNumber((tempGasCost + insuranceCost + maintCost + permitCost));
        } else if (permitChoice.equals("Daily Parking")) {
            permitCost = calculate.monthCost(temp, savedDays);
            mTotal = calculate.roundNumber((tempGasCost + insuranceCost + maintCost + permitCost));
        } else {
            mTotal = calculate.roundNumber((tempGasCost + insuranceCost + maintCost));
        }


        if (option.equals("Single Occupant Vehicle")) {
            s = costString[0] + twoWay + "\n" + savedDays + " " + costString[1] + weekCost
                    + costString[2] + monthCost + costString[3] + insuranceCost
                    + costString[4] + maintCost + "\nYou have chosen " + permitChoice + " as your permit option";
            if (permitChoice.equals("Weekly Permit")) {
                s = s + "\nFor a month, you will pay - $" + permitCost;
            } else if (permitChoice.equals("Daily Parking")) {
                s = s + "\nFor " + savedDays + " times a week, you will spend - $" + permitCost + " per month.";
            } else {
                s = s + "\nThis will not be included in your monthly total but your permit cost - $" + permitCost;
            }
            s = s + costString[5] + mTotal + costString[6];
        } else if (option.equals("Multiple Occupant Vehicle")) {
            String pCost = "\nBut, with " + occupantNumber + " people carpooling with you, you can " +
                    "split the permit cost equally to about: $" + calculate.roundNumber(permitCost / occupantNumber) + " per person.";
            //string to output the results
            s = costString[0] + twoWay + "\n" + savedDays + " " + costString[1] + weekCost
                    + costString[2] + monthCost + costString[3] + insuranceCost
                    + costString[4] + maintCost + "\nYou have chosen " + permitChoice + " as your permit option";
            if (permitChoice.equals("Weekly Permit")) {
                s = s + "\nFor a month, you will pay - $" + permitCost + pCost;
            } else if (permitChoice.equals("Daily Parking")) {
                s = s + "\nFor " + savedDays + " times a week, you will spend - $" + permitCost + " per month." + pCost;
            } else {
                s = s + "\nThis will not be included in your monthly total but your permit cost - $" + permitCost + pCost;
            }
            s = s + costString[5] + mTotal + costString[6];
        }


        return s;
    }
    public static String busRailString(String role, double dailyCost, double savedDays){
        String s = "";
        if(role.equals("Student")){
            s = "Since you are a student and have the commuter sleeve, transit costs are covered by your" +
                    " registration fees.\nKeep in mind of any additional costs such as routes to go to the" +
                    "bus stop and such.";
        }else if (role.equals("Faculty")){
            s = "Since you are a faculty member, you will need to purchase a $40 commuter sleeve with your" +
                    "  OneCard either at the Bursar's Office or UTAPS Office.\nKeep in mind of any additional costs such " +
                    "as routes to go to the bus stop and such.";
        }else {
            s = "If you choose to pay daily, you will be paying $" + (dailyCost * savedDays) + " per week\n" +
                    "For the month, $" + ((dailyCost * savedDays) * 4) + " per month\n" +
                    "It may be worth to purchase a monthly pass if you're committing to use it for the entire month";
        }

        return s;
    }

    public static String walkString() {
        return costString[8];
    }

    public static String rideShareString(String choice, double fare, double mileRate, double minuteRate, double serviceCost, float distance, int days) {
        String s = "";
        double timeResidential = Time.travelTime(distance, 45);
        int resMin = Time.getMinuteB(timeResidential);

        double timeFreeway = Time.travelTime(distance, 65);
        int freeMin = Time.getMinuteB(timeFreeway);

        double mileCost = mileRate * (double) distance;
        double minuteRateR = minuteRate * resMin;
        double minuteRateF = minuteRate * freeMin;

        double TotalA = fare + minuteRateR + mileCost + serviceCost;
        double TotalB = fare + minuteRateF + mileCost + serviceCost;

        s = "You have chosen, " + choice + ".\nYou estimated rate will vary between $" + Math.round(TotalB) + " and $" + Math.round(TotalA) + "*." +
                "\nIf you rideShare " + days + " days a week, you will spend around $" + (Math.round(TotalB) * days) + " to $" + (Math.round(TotalA) * days) + " per week." +
                "\nAnd if you do it per month, it will cost around $" + ((Math.round(TotalB) * days) * 4) + " to $" + ((Math.round(TotalA) * days) * 4) + " per month." +
                "\nYou will not have to pay for insurance, gas, maintenance, and parking permit." +
                "\n\n*Keep in mind of cancellation fees as-well as minimum and maximum fares. This is only an estimate since it may not factor the speed and condition of traffic, discounts, and other factors.";


        return s;
    }


    public static String sustainabilityString(String option, double MPG, double distance, int occupantNumber) {
        String temp, compare = "";
        double cEmission = calculate.carbonEmission(MPG, distance);
        double emitLb = calculate.gramsToLbs(cEmission);
        if ((option.equals(commuteTitle[0][0]) || (option.equals(commuteTitle[2][0])))) { //if single occupant vehicle or motorcycle
            compare = "\n\nIf you were to carpool instead, your emission rate would be 1/4 of your current " +
                    "emission rate: " + calculate.roundNumber(cEmission / 4) + " grams or " +
                    calculate.roundNumber(emitLb / 4) + " lbs." + "\n\n" + calculate.roundNumber(cEmission - (cEmission / 4))
                    + " grams less than your current commute option.";
        } else if (option.equals(commuteTitle[1][0])) { //if multiple occupant vehicle
            compare = "\n\nIf you were to drive by yourself instead, your emission rate would be the same" +
                    ", but if your " + occupantNumber + " passengers drove themselves, your emission rate will increase " + occupantNumber +
                    " times your current emission rate emitting (assuming they are driving similar vehicles with the " +
                    "same MPG and distance): " + calculate.roundNumber(cEmission * occupantNumber) + " grams or " +
                    calculate.roundNumber(emitLb * occupantNumber) + "lbs. of carbon dioxide all together.";
        } else if (option.equals(commuteTitle[3][0])) {
            compare = "\n\nAlthough it may not be ideal to take the bus if the length of the route is very long and requires" +
                    " you to take multiple commute options to get from a station to another. The bus can save the amount of " +
                    "traffic congestion thus reducing the amount of CO2 emitted in the air\nAssuming the car has a MPG of "+ MPG +
                    " and driving the " +"same distance of "+ Math.round(distance) + " miles, " + "carbon rates for 15 cars would be " +
                     Math.round(calculate.carbonEmission(25.6, distance)) + " grams against a bus whose carbon emission rate is " + cEmission +
                    " grams, " + (Math.round(calculate.carbonEmission(25.6, distance)) - cEmission) + " grams less." +
                    "Although, it may not be ideal to take a transit bus if the expected travel time is very long.\n" +
                    "If you are taking the light rail, it is 100% electric and not emitting carbon dioxide into the air at all!";

        }else if (option.equals(commuteTitle[4][0])) {
            compare = "\n\nBesides from the already said cons, walking is the best to save money from car costs, and " +
                    "congesting (and dealing with) traffic. Compared to driving a car, a  car will emit " + Math.round(calculate.carbonEmission(25.4, distance)) +
                     " grams of CO2 compared to walking which will emit almost nothing";
        } else if (option.equals(commuteTitle[5][0])) {
            compare =  "\n\nBesides from the already said cons, walking is the best to save money from car costs, and " +
                    "congesting (and dealing with) traffic. Compared to driving a car, a  car will emit " + Math.round(calculate.carbonEmission(25.4, distance)) +
                    " grams of CO2 compared to biking which will emit almost nothing";
        } else if (option.equals(commuteTitle[6][0])) {
            compare = "\n\nSimilar to if you were carpooling, If you rideShare with other students, at least 2, your " +
                    "carbon emissions would be reduced by half by reducing the amount of cars in traffic." +
                    "\nHowever, if you are rideSharing yourself, unless in certain situations, it's best not to" +
                    "use rideSharing services. It will still emit a significant amount of carbon dioxide, the same amount if you were" +
                    "driving yourself, and increase traffic congestion.\nRideShare only if necessary if you cannot drive or walk to campus yourself";
            if (distance < 2.0) {
                compare = compare + "\n\nMay I suggest to ride a bike to campus? You live within a walkable distance of" +
                        distance + " miles. It emits little to no carbon dioxide and is much faster than walking! Check" +
                        " the bike option for more info.";
            } else if(distance > 2.0){
                compare = compare + "\n\nMaybe take a looking at other transit options such as bus and/or light rail. Although a higher" +
                        "MPG, if we push people to take advantage of public transportation, we can remove at least 15 cars per bus from the" +
                        "streets, freeing traffic.\nAssuming the car has a MPG of "+ MPG + " and driving the " +"same distance of "+ Math.round(distance) + " miles, " +
                        "carbon rates for 15 cars would be " + Math.round((cEmission * 15)) + " grams against a bus whose carbon emission rate is " + Math.round(calculate.carbonEmission(7.2, distance)) +
                        " grams, " + (Math.round((cEmission * 15)) - Math.round(calculate.carbonEmission(7.2, distance))) + " grams less." +
                        "Although, it may not be ideal to take a transit bus if the expected travel time is very long.";
            }
        }


        temp = "By choosing, " + option + ", as your commute option,\n" + cEmission + " grams of " +
                "carbon dioxide is emitted into the air (two-way).\n\nThat is about " + calculate.roundNumber(emitLb) + " lbs. " +
                 compare;

        return temp;
    }





    public static String setDesc(String a){
        String s = "";
        for(int i = 0; i <= commuteTitle.length; i++){
            String temp = "";
            temp = commuteTitle[i][0];
            if(a.equals(temp)){
                s = commuteTitle[i][1];
                break;
            }
        }
        return s;
    }


}
